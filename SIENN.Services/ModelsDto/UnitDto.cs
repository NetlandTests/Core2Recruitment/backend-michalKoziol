﻿using SIENN.DbAccess.Models;

namespace SIENN.Services.ModelsDto
{
	public class UnitDto
	{
		public string Code { get; set; }

		public string Description { get; set; }

		public UnitDto()
		{

		}

		public UnitDto(Unit unit)
		{
			Code = unit.Code;
			Description = unit.Description;
		}

		public Unit ToUnit()
		{
			return new Unit()
			{
				Code = Code,
				Description = Description
			};
		}
	}
}