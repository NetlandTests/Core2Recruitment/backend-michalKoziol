﻿using System;
using System.Collections.Generic;
using SIENN.DbAccess.Models;

namespace SIENN.Services.ModelsDto
{
	public class ProductDto
	{
		public string Code { get; set; }

		public string Description { get; set; }

		public decimal Price { get; set; }

		public bool IsAvailable { get; set; }

		public DateTime DeliveryDate { get; set; }

		public TypeDto Type { get; set; }

		public UnitDto Unit { get; set; }

		public ICollection<CategoryDto> Categories { get; set; }

		public Product ToProduct()
		{
			return new Product()
			{
				Code = Code,
				Description = Description,
				DeliveryDate = DeliveryDate,
				IsAvailable = IsAvailable,
				Price = Price,
				TypeCode = Type.Code,
				UnitCode = Unit.Code,
				ProductCategories = new List<ProductCategories>()
			};
		}
	}
}
