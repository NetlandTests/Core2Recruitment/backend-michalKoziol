﻿using SIENN.DbAccess.Models;

namespace SIENN.Services.ModelsDto
{
	public class CategoryDto
	{
		public string Code { get; set; }

		public string Description { get; set; }

		public CategoryDto()
		{

		}

		public CategoryDto(Category category)
		{
			Code = category.Code;
			Description = category.Description;
		}

		public Category ToCategory()
		{
			return new Category()
			{
				Description = Description,
				Code = Code
			};
		}
	}
}