﻿using SIENN.DbAccess.Models;

namespace SIENN.Services.ModelsDto
{
	public class TypeDto
	{
		public string Code { get; set; }

		public string Description { get; set; }

		public TypeDto()
		{

		}

		public TypeDto(Type type)
		{
			Code = type.Code;
			Description = type.Description;
		}

		public Type ToType()
		{
			return new Type()
			{
				Code = Code,
				Description = Description
			};
		}
	}
}