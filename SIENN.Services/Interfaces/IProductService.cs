﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using SIENN.DbAccess.Models;
using SIENN.Services.ModelsDto;

namespace SIENN.Services.Interfaces
{
    public interface IProductService
	{
		ProductDto Get(string code);

		IEnumerable<ProductDto> Find(Expression<Func<Product, bool>> predicate);
		IEnumerable<ProductDto> GetAll();
		IResultSet<ProductDto> GetAvailableProducts(int page);
		IResultSet<ProductDto> GetFilteredProducts(int page, string unitCode, string categoryCode, string typeCode);

		int Count();

		void Add(ProductDto product);
		void Remove(ProductDto product);
		void Update(ProductDto productDto);
	}
}
