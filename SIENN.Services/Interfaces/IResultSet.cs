﻿using System.Collections.Generic;
using SIENN.Services.Navigation;

namespace SIENN.Services.Interfaces
{
	public interface IResultSet<T>
	{
		IEnumerable<T> Items { get; set; }

		Pager Pager { get; }
	}
}