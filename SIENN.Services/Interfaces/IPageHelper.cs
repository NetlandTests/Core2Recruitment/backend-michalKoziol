﻿using System.Collections.Generic;

namespace SIENN.Services.Interfaces
{
	public interface IPageHelper<T>
	{
		IResultSet<T> GetPage(IEnumerable<T> items, int pageNumber);
	}
}