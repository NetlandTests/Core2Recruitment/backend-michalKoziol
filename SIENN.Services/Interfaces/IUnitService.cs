﻿using SIENN.Services.ModelsDto;

namespace SIENN.Services.Interfaces
{
	public interface IUnitService
	{
		void Add(UnitDto unitDto);
		void Delete(UnitDto unitDto);
		UnitDto Get(string code);
		void Update(UnitDto unitDto);
	}
}