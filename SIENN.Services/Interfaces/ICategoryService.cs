﻿using SIENN.Services.ModelsDto;

namespace SIENN.Services.Interfaces
{
	public interface ICategoryService
	{
		void Add(CategoryDto categoryDto);
		void Delete(CategoryDto categoryDto);
		CategoryDto Get(string code);
		void Update(CategoryDto categoryDto);
	}
}