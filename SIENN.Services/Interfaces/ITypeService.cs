﻿using SIENN.Services.ModelsDto;

namespace SIENN.Services.Interfaces
{
	public interface ITypeService
	{
		void Add(TypeDto typeDto);
		void Delete(TypeDto typeDto);
		TypeDto Get(string code);
		void Update(TypeDto typeDto);
	}
}