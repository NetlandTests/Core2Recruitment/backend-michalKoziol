﻿using System.Linq;
using SIENN.DbAccess;
using SIENN.Services.Interfaces;
using SIENN.Services.ModelsDto;

namespace SIENN.Services.Services
{
	public class CategoryService : ICategoryService
	{
		private readonly ApplicationDbContext _context;

		public CategoryService(ApplicationDbContext context)
		{
			_context = context;
		}

		public CategoryDto Get(string code)
		{
			var category = _context.Categories.Find(code);
			return new CategoryDto(category);
		}

		public void Add(CategoryDto categoryDto)
		{
			if (_context.Categories.Any(x => x.Code == categoryDto.Code)) return;

			_context.Categories.Add(categoryDto.ToCategory());
			_context.SaveChanges();
		}

		public void Update(CategoryDto categoryDto)
		{
			var originalCategory = _context.Categories.Find(categoryDto.Code);

			if (originalCategory == null) return;

			originalCategory.Description = categoryDto.Description;
			_context.SaveChanges();
		}

		public void Delete(CategoryDto categoryDto)
		{
			var categoryToDelete = _context.Categories.Find(categoryDto.Code);

			if (categoryToDelete == null) return;

			_context.Categories.Remove(categoryToDelete);
		}
	}
}
