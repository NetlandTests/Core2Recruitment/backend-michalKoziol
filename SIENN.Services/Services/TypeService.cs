﻿using System.Linq;
using SIENN.DbAccess;
using SIENN.Services.Interfaces;
using SIENN.Services.ModelsDto;

namespace SIENN.Services.Services
{
	public class TypeService : ITypeService
	{
		private readonly ApplicationDbContext _context;

		public TypeService(ApplicationDbContext context)
		{
			_context = context;
		}

		public TypeDto Get(string code)
		{
			var type = _context.Types.Find(code);
			return new TypeDto(type);
		}

		public void Add(TypeDto typeDto)
		{
			if (_context.Types.Any(x => x.Code == typeDto.Code)) return;

			_context.Types.Add(typeDto.ToType());
			_context.SaveChanges();
		}

		public void Update(TypeDto typeDto)
		{
			var originalType = _context.Types.Find(typeDto.Code);

			if (originalType == null) return;

			originalType.Description = typeDto.Description;
			_context.SaveChanges();
		}

		public void Delete(TypeDto typeDto)
		{
			var typeToDelete = _context.Types.Find(typeDto.Code);

			if (typeToDelete == null) return;

			_context.Types.Remove(typeToDelete);
		}
	}
}
