﻿using System.Linq;
using SIENN.DbAccess;
using SIENN.Services.Interfaces;
using SIENN.Services.ModelsDto;

namespace SIENN.Services.Services
{
	public class UnitService : IUnitService
	{
		private readonly ApplicationDbContext _context;

		public UnitService(ApplicationDbContext context)
		{
			_context = context;
		}

		public UnitDto Get(string code)
		{
			var unit = _context.Units.Find(code);
			return new UnitDto(unit);
		}

		public void Add(UnitDto unitDto)
		{
			if (_context.Units.Any(x => x.Code == unitDto.Code)) return;

			_context.Units.Add(unitDto.ToUnit());
			_context.SaveChanges();
		}

		public void Update(UnitDto unitDto)
		{
			var originalUnit = _context.Units.Find(unitDto.Code);

			if (originalUnit == null) return;

			originalUnit.Description = unitDto.Description;
			_context.SaveChanges();
		}

		public void Delete(UnitDto unitDto)
		{
			var UnitToDelete = _context.Units.Find(unitDto.Code);

			if (UnitToDelete == null) return;

			_context.Units.Remove(UnitToDelete);
		}
	}
}
