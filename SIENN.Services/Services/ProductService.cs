﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using SIENN.Services.Interfaces;
using SIENN.Services.ModelsDto;
using SIENN.DbAccess;
using Microsoft.EntityFrameworkCore;
using SIENN.DbAccess.Models;

namespace SIENN.Services.Services
{
	public class ProductService : IProductService
	{
		private readonly ApplicationDbContext _context;
		private readonly IPageHelper<ProductDto> _pageHelper;
		private readonly IUnitService _unitService;
		private readonly ITypeService _typeService;

		public ProductService(ApplicationDbContext context, IPageHelper<ProductDto> pageHelper, IUnitService unitService, ITypeService typeService)
		{
			_context = context;
			_pageHelper = pageHelper;
			_unitService = unitService;
			_typeService = typeService;
		}

		public ProductDto Get(string code)
		{
			var query = _context.Products.Include(x => x.ProductCategories).ThenInclude(x => x.Category);
			var product = query.FirstOrDefault(x => x.Code == code);
			var unit = _context.Units.Find(product.UnitCode);
			var type = _context.Types.Find(product.TypeCode);

			var productDto = new ProductDto()
			{
				Code = product.Code,
				Description = product.Description,
				DeliveryDate = product.DeliveryDate,
				IsAvailable = product.IsAvailable,
				Price = product.Price,
				Categories = new List<CategoryDto>(),
				Unit = new UnitDto()
				{
					Code = unit.Code,
					Description = unit.Description
				},
				Type = new TypeDto()
				{
					Code = type.Code,
					Description = type.Description
				}
			};

			foreach (var prodCategories in product.ProductCategories)
			{
				productDto.Categories.Add(new CategoryDto()
				{
					Code = prodCategories.Category.Code,
					Description = prodCategories.Category.Description
				});
			}

			return productDto;
		}

		public IEnumerable<ProductDto> GetAll()
		{
			var result = new List<ProductDto>();

			foreach (var product in _context.Products)
			{
				result.Add(Get(product.Code));
			}

			return result;
		}

		public IResultSet<ProductDto> GetAvailableProducts(int page)
		{
			var availableProducts = new List<ProductDto>();

			foreach (var product in _context.Products)
			{
				if (product.IsAvailable)
					availableProducts.Add(Get(product.Code));
			}

			var result = _pageHelper.GetPage(availableProducts, page);

			return result;
		}

		public IResultSet<ProductDto> GetFilteredProducts(int page, string unitCode, string categoryCode, string typeCode)
		{
			var filteredProductsDto = new List<ProductDto>();

			var filteredProducts = Find(x => (typeCode == null || x.TypeCode.ToUpper() == typeCode.ToUpper()) &&
									(unitCode == null || x.UnitCode.ToUpper() == unitCode.ToUpper()) &&
									(categoryCode == null || x.ProductCategories.Any(c => c.CategoryCode.ToUpper() == categoryCode.ToUpper()))
			);


			foreach (var product in filteredProducts)
			{
				if (product.IsAvailable)
					filteredProductsDto.Add(Get(product.Code));
			}

			var result = _pageHelper.GetPage(filteredProductsDto, page);

			return result;
		}

		public virtual IEnumerable<ProductDto> Find(Expression<Func<Product, bool>> predicate)
		{
			var products = _context.Products.Where(predicate);

			var result = new List<ProductDto>();

			foreach (var product in products)
			{
				result.Add(Get(product.Code));
			}

			return result;
		}

		public virtual int Count()
		{
			return _context.Products.Count();
		}

		public virtual void Add(ProductDto productDto)
		{
			var product = productDto.ToProduct();


			_unitService.Add(productDto.Unit);

			_typeService.Add(productDto.Type);

			if (_context.Products.All(x => x.Code != product.Code))
				_context.Products.Add(product);


			if (productDto.Categories != null)
			{
				foreach (var categoryDto in productDto.Categories)
				{
					var category = categoryDto.ToCategory();

					if (_context.Categories.All(x => x.Code != category.Code))
						_context.Categories.Add(category);

					var productCategories = new ProductCategories()
					{
						Product = product,
						Category = category
					};
					if (_context.ProductCategories.Any(x => x.CategoryCode == category.Code && x.ProductCode == product.Code)) continue;
					_context.ProductCategories.Add(productCategories);
				}
			}

			_context.SaveChanges();
		}

		public virtual void Remove(ProductDto productDto)
		{
			var query = _context.Products.Include(x => x.ProductCategories).ThenInclude(x => x.Category);
			var entity = query.FirstOrDefault(x => x.Code == productDto.Code);
			_context.Products.Remove(entity);
		}

		public void Update(ProductDto productDto)
		{
			var query = _context.Products.Include(x => x.ProductCategories).ThenInclude(x => x.Category);
			var product = query.FirstOrDefault(x => x.Code == productDto.Code);

			if (product != null)
			{
				product.Description = productDto.Description;
				product.DeliveryDate = productDto.DeliveryDate;
				product.IsAvailable = productDto.IsAvailable;
				product.Price = productDto.Price;
				product.Description = productDto.Description;
			}

			_context.SaveChanges();
		}
	}
}
