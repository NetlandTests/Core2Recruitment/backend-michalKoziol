﻿using System;
using System.Collections.Generic;
using System.Linq;
using SIENN.Services.Interfaces;

namespace SIENN.Services.Navigation
{
	public class PageHelper<T> : IPageHelper<T>
	{
		public int PageSize { get; set; } = 20;

		public IResultSet<T> GetPage(IEnumerable<T> items, int pageNumber)
		{
			var numberOfRecords = items.Count();
			var numberOfPages = GetPaggingCount(numberOfRecords, PageSize);

			if (pageNumber == 0) { pageNumber = 1; }

			var pager = new Pager
			{
				NumberOfPages = numberOfPages,
				CurrentPage = pageNumber,
				TotalRecords = numberOfRecords
			};

			var countFrom = _countFrom(PageSize, pageNumber);

			var resultSet = new ResultSet<T>
			{
				Pager = pager,
				Items = items.Skip(countFrom).Take(PageSize)
			};

			return resultSet;
		}

		private readonly Func<int, int, int> _countFrom =
			(pageSize, pageNumber) => pageNumber == 1 ? 0 : (pageSize * pageNumber) - pageSize;

		private static int GetPaggingCount(int count, int pageSize)
		{
			var extraCount = count % pageSize > 0 ? 1 : 0;
			return (count < pageSize) ? 1 : (count / pageSize) + extraCount;
		}

		public class ResultSet<T> : IResultSet<T>
		{
			public IEnumerable<T> Items { get; set; }
			public Pager Pager { get; set; }
		}
	}
}
