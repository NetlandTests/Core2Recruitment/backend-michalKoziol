﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SIENN.Services.Navigation
{
	public class Pager
	{
		public int NumberOfPages { get; set; }

		public int CurrentPage { get; set; }

		public int TotalRecords { get; set; }
	}
}
