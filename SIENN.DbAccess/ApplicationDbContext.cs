﻿using Microsoft.EntityFrameworkCore;
using SIENN.DbAccess.Models;

namespace SIENN.DbAccess
{
    public class ApplicationDbContext : DbContext
	{
		public ApplicationDbContext(DbContextOptions options) : base(options)
		{

		}

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			modelBuilder.Entity<ProductCategories>()
				.HasKey(t => new {t.ProductCode, t.CategoryCode});
			modelBuilder.Entity<ProductCategories>().HasOne(x => x.Product).WithMany(y => y.ProductCategories)
				.HasForeignKey(x => x.ProductCode);
			modelBuilder.Entity<ProductCategories>().HasOne(x => x.Category).WithMany(y => y.ProductCategories)
				.HasForeignKey(x => x.CategoryCode);

			modelBuilder.Entity<Unit>().HasMany(x => x.Products).WithOne().HasForeignKey(y => y.UnitCode);
			modelBuilder.Entity<Type>().HasMany(x => x.Products).WithOne().HasForeignKey(y => y.TypeCode);
		}

		public DbSet<Product> Products { get; set; }
		public DbSet<Category> Categories { get; set; }
		public DbSet<Type> Types { get; set; }
		public DbSet<Unit> Units { get; set; }
		public DbSet<ProductCategories> ProductCategories { get; set; }
	}
}
