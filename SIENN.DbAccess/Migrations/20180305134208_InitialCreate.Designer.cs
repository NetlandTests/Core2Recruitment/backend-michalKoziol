﻿// <auto-generated />
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.EntityFrameworkCore.Storage.Internal;
using SIENN.DbAccess;
using System;

namespace SIENN.DbAccess.Migrations
{
    [DbContext(typeof(ApplicationDbContext))]
    [Migration("20180305134208_InitialCreate")]
    partial class InitialCreate
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.0.1-rtm-125")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("SIENN.Services.Models.Category", b =>
                {
                    b.Property<string>("Code")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("varchar(10)");

                    b.Property<string>("Description")
                        .HasColumnType("varchar(50)");

                    b.HasKey("Code");

                    b.ToTable("Categories");
                });

            modelBuilder.Entity("SIENN.Services.Models.Product", b =>
                {
                    b.Property<string>("Code")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("varchar(10)");

                    b.Property<DateTime>("DeliveryDate");

                    b.Property<string>("Description")
                        .HasColumnType("varchar(50)");

                    b.Property<bool>("IsAvailable");

                    b.Property<double>("Price")
                        .HasColumnType("decimal(10, 2)");

                    b.Property<string>("TypeCode");

                    b.Property<string>("UnitCode");

                    b.HasKey("Code");

                    b.HasIndex("TypeCode");

                    b.HasIndex("UnitCode");

                    b.ToTable("Products");
                });

            modelBuilder.Entity("SIENN.Services.Models.ProductCategories", b =>
                {
                    b.Property<string>("ProductCode");

                    b.Property<string>("CategoryCode");

                    b.HasKey("ProductCode", "CategoryCode");

                    b.HasIndex("CategoryCode");

                    b.ToTable("ProductCategories");
                });

            modelBuilder.Entity("SIENN.Services.Models.Type", b =>
                {
                    b.Property<string>("Code")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("varchar(10)");

                    b.Property<string>("Description")
                        .HasColumnType("varchar(50)");

                    b.HasKey("Code");

                    b.ToTable("Types");
                });

            modelBuilder.Entity("SIENN.Services.Models.Unit", b =>
                {
                    b.Property<string>("Code")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("varchar(10)");

                    b.Property<string>("Description")
                        .HasColumnType("varchar(50)");

                    b.HasKey("Code");

                    b.ToTable("Units");
                });

            modelBuilder.Entity("SIENN.Services.Models.Product", b =>
                {
                    b.HasOne("SIENN.Services.Models.Type", "Type")
                        .WithMany("Products")
                        .HasForeignKey("TypeCode");

                    b.HasOne("SIENN.Services.Models.Unit", "Unit")
                        .WithMany("Products")
                        .HasForeignKey("UnitCode");
                });

            modelBuilder.Entity("SIENN.Services.Models.ProductCategories", b =>
                {
                    b.HasOne("SIENN.Services.Models.Category", "Category")
                        .WithMany("ProductCategories")
                        .HasForeignKey("CategoryCode")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("SIENN.Services.Models.Product", "Product")
                        .WithMany("ProductCategories")
                        .HasForeignKey("ProductCode")
                        .OnDelete(DeleteBehavior.Cascade);
                });
#pragma warning restore 612, 618
        }
    }
}
