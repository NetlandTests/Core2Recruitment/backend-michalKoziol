﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SIENN.DbAccess.Models
{
	public class Category
	{
		[Key, Column(TypeName = "varchar(10)")]
		public string Code { get; set; }

		[Column(TypeName = "varchar(50)")]
		public string Description { get; set; }

		public IEnumerable<ProductCategories> ProductCategories { get; set; }
	}
}
