﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SIENN.DbAccess.Models
{
	public class Product
	{
		[Key, Column(TypeName = "varchar(10)")]
		public string Code { get; set; }

		[Column(TypeName = "varchar(50)")]
		public string Description { get; set; }

		[Column(TypeName = "decimal(10, 2)")]
		public decimal Price { get; set; }

		public bool IsAvailable { get; set; }

		public DateTime DeliveryDate { get; set; }
		
		public string TypeCode { get; set; }

		public string UnitCode { get; set; }

		public IEnumerable<ProductCategories> ProductCategories { get; set; }
	}
}
