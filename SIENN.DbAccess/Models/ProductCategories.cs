﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SIENN.DbAccess.Models
{
	public class ProductCategories
	{
		[Key]
		public string ProductCode { get; set; }

		public Product Product { get; set; }

		[Key]
		public string CategoryCode { get; set; }
		public Category Category { get; set; }
	}
}
