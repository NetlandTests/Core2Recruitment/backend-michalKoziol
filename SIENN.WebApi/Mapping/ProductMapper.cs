﻿using SIENN.Services.Models;
using SIENN.Services.ModelsDto;

namespace SIENN.WebApi.Mapping
{
	public static class ProductMapper
	{
		public static ProductViewModel MapToProductViewModel(ProductDto productDto)
		{
			var categoriesCount = 0;

			if (productDto.Categories != null)
			{
				categoriesCount = productDto.Categories.Count;
			}

			var productVm = new ProductViewModel()
			{
				ProductDescription = $"({productDto.Code}) {productDto.Description}",
				Price = productDto.Price.ToString("C"),
				IsAvailable = productDto.IsAvailable ? "Dostępny" : "Niedostępny",
				DeliveryDate = productDto.DeliveryDate.ToShortDateString(),
				CategoriesCount = categoriesCount,
				Type = $"({productDto.Type.Code}) {productDto.Type.Description}",
				Unit = $"({productDto.Unit.Code}) {productDto.Unit.Description}"
			};

			return productVm;
		}
	}
}
