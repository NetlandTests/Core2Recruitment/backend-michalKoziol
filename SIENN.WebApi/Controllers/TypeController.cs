﻿using System;
using System.Collections.Generic;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SIENN.Services.Interfaces;
using SIENN.Services.Models;
using SIENN.Services.ModelsDto;
using SIENN.WebApi.Mapping;

namespace SIENN.WebApi.Controllers
{
    [Route("api/[controller]")]
    public class TypeController : Controller
    {
	    private readonly ITypeService _typeService;
	    private readonly ILogger<TypeController> _logger;

	    public TypeController(ITypeService typeService, ILogger<TypeController> logger)
	    {
		    _typeService = typeService;
		    _logger = logger;
	    }

	    [HttpGet("GetType")]
	    public IActionResult GetType(string code)
	    {
		    try
		    {
			    var type = _typeService.Get(code);

			    if (type == null)
			    {
				    return StatusCode((int)HttpStatusCode.NotFound);
			    }

			    return Ok(type);
		    }
		    catch (Exception ex)
			{
				_logger.LogError(ex, $"Error on getting type for code: {code}", code);
				return StatusCode((int)HttpStatusCode.InternalServerError, "Error on Getting type" + Environment.NewLine + ex.ToString());
		    }
	    }

	    [HttpPost("CreateType")]
	    public IActionResult CreateType(TypeDto type)
	    {
		    try
		    {
			    _typeService.Add(type);

			    return Ok();
		    }
		    catch (Exception ex)
			{
				_logger.LogError(ex, $"Error on Creating type", type);
				return StatusCode((int)HttpStatusCode.InternalServerError, "Error on Creating type:" + Environment.NewLine + ex.ToString());
		    }
	    }

	    [HttpPut("EditType")]
	    public IActionResult EditType(TypeDto type)
	    {
		    try
		    {
			    _typeService.Update(type);

			    return Ok();
		    }
		    catch (Exception ex)
			{
				_logger.LogError(ex, $"Error on Updating type", type);
				return StatusCode((int)HttpStatusCode.InternalServerError, "Error on Updating type" + Environment.NewLine + ex.ToString());
		    }
	    }

	    [HttpDelete("DeleteType")]
	    public IActionResult DeleteType(TypeDto type)
	    {
		    try
		    {
			    _typeService.Delete(type);

			    return Ok();
		    }
		    catch (Exception ex)
			{
				_logger.LogError(ex, $"Error on Deleting type", type);
				return StatusCode((int)HttpStatusCode.InternalServerError, "Error on Deleting type" + Environment.NewLine + ex.ToString());
		    }
	    }
	}
}
