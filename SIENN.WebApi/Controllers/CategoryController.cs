﻿using System;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SIENN.Services.Interfaces;
using SIENN.Services.ModelsDto;

namespace SIENN.WebApi.Controllers
{
    [Route("api/[controller]")]
    public class CategoryController : Controller
    {
		private readonly ICategoryService _categoryService;
	    private readonly ILogger<CategoryController> _logger;

	    public CategoryController(ICategoryService categoryService, ILogger<CategoryController> logger)
	    {
		    _categoryService = categoryService;
		    _logger = logger;
	    }

	    [HttpGet("GetCategory")]
	    public IActionResult GetCategory(string code)
	    {
		    try
		    {
			    var category = _categoryService.Get(code);

			    if (category == null)
			    {
				    return StatusCode((int)HttpStatusCode.NotFound);
			    }

			    return Ok(category);
		    }
		    catch (Exception ex)
			{
				_logger.LogError(ex, $"Error on Getting category for code: {code}", code);
				return StatusCode((int)HttpStatusCode.InternalServerError, "Error on Getting category" + Environment.NewLine + ex.ToString());
		    }
	    }

	    [HttpPost("CreateCategory")]
	    public IActionResult CreateCategory(CategoryDto category)
	    {
		    try
		    {
			    _categoryService.Add(category);

			    return Ok();
		    }
		    catch (Exception ex)
			{
				_logger.LogError(ex, $"Error on Creating category", category);
				return StatusCode((int)HttpStatusCode.InternalServerError, "Error on Creating category:" + Environment.NewLine + ex.ToString());
		    }
	    }

	    [HttpPut("EditCategory")]
	    public IActionResult EditCategory(CategoryDto category)
	    {
		    try
		    {
			    _categoryService.Update(category);

			    return Ok();
		    }
		    catch (Exception ex)
			{
				_logger.LogError(ex, $"Error on Updating category", category);
				return StatusCode((int)HttpStatusCode.InternalServerError, "Error on Updating category" + Environment.NewLine + ex.ToString());
		    }
	    }

	    [HttpDelete("DeleteCategory")]
	    public IActionResult DeleteCategory(CategoryDto category)
	    {
		    try
		    {
			    _categoryService.Delete(category);

			    return Ok();
		    }
		    catch (Exception ex)
			{
				_logger.LogError(ex, $"Error on Deleting category", category);
				return StatusCode((int)HttpStatusCode.InternalServerError, "Error on Deleting category" + Environment.NewLine + ex.ToString());
		    }
	    }
	}
}
