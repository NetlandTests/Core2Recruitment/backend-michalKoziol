﻿using System;
using System.Collections.Generic;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SIENN.DbAccess.Migrations;
using SIENN.Services.Interfaces;
using SIENN.Services.Models;
using SIENN.Services.ModelsDto;
using SIENN.WebApi.Mapping;

namespace SIENN.WebApi.Controllers
{
	[Route("api/[controller]")]
	public class UnitController : Controller
	{
		private readonly IUnitService _unitService;
		private readonly ILogger<UnitController> _iLogger;

		public UnitController(IUnitService unitService, ILogger<UnitController> iLogger)
		{
			_unitService = unitService;
			_iLogger = iLogger;
		}

		[HttpGet("GetUnit")]
		public IActionResult GetUnit(string code)
		{
			try
			{
				var unit = _unitService.Get(code);

				if (unit == null)
				{
					return StatusCode((int)HttpStatusCode.NotFound);
				}

				return Ok(unit);
			}
			catch (Exception ex)
			{
				_iLogger.LogError(ex, $"Error on getting Unit for code: {code}", code);
				return StatusCode((int)HttpStatusCode.InternalServerError, "Error on Getting Unit" + Environment.NewLine + ex.ToString());
			}
		}

		[HttpPost("CreateUnit")]
		public IActionResult CreateUnit(UnitDto unit)
		{
			try
			{
				_unitService.Add(unit);

				return Ok();
			}
			catch (Exception ex)
			{
				_iLogger.LogError(ex, $"Error on Creating Unit ", unit);
				return StatusCode((int)HttpStatusCode.InternalServerError, "Error on Creating Unit:" + Environment.NewLine + ex.ToString());
			}
		}

		[HttpPut("EditUnit")]
		public IActionResult EditUnit(UnitDto unit)
		{
			try
			{
				_unitService.Update(unit);

				return Ok();
			}
			catch (Exception ex)
			{
				_iLogger.LogError(ex, $"Error on Updating Unit ", unit);
				return StatusCode((int)HttpStatusCode.InternalServerError, "Error on Updating Unit" + Environment.NewLine + ex.ToString());
			}
		}

		[HttpDelete("DeleteUnit")]
		public IActionResult Deleteunit(UnitDto unit)
		{
			try
			{
				_unitService.Delete(unit);

				return Ok();
			}
			catch (Exception ex)
			{
				_iLogger.LogError(ex, $"Error on Deleting Unit ", unit);
				return StatusCode((int)HttpStatusCode.InternalServerError, "Error on Deleting Unit" + Environment.NewLine + ex.ToString());
			}
		}
	}
}
