﻿using System;
using System.Linq;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SIENN.Services.Interfaces;
using SIENN.Services.ModelsDto;
using SIENN.WebApi.Mapping;

namespace SIENN.WebApi.Controllers
{
	[Route("api/[controller]")]
	public class ProductController : Controller
	{
		private readonly IProductService _productService;
		private readonly ILogger<ProductController> _logger;

		public ProductController(IProductService productService, ILogger<ProductController> logger)
		{
			_productService = productService;
			_logger = logger;
		}

		[HttpGet("GetProduct")]
		public IActionResult GetProduct(string code)
		{
			try
			{
				var product = _productService.Get(code);

				if (product == null) return StatusCode((int)HttpStatusCode.NotFound);

				return Ok(product);
			}
			catch (Exception ex)
			{
				_logger.LogError(ex, $"Error on Getting Product for code: {code}", code);
				return StatusCode((int)HttpStatusCode.InternalServerError, "Error on Getting Product" + Environment.NewLine + ex.ToString());
			}
		}

		[HttpGet("GetProductDetails")]
		public IActionResult GetProductDetails(string code)
		{
			try
			{
				var product = _productService.Get(code);

				if (product == null) return StatusCode((int)HttpStatusCode.NotFound);

				return Ok(ProductMapper.MapToProductViewModel(product));
			}
			catch (Exception ex)
			{
				_logger.LogError(ex, $"Error on Getting Product Details for code: {code}", code);
				return StatusCode((int)HttpStatusCode.InternalServerError, "Error on Getting Product Details" + Environment.NewLine + ex.ToString());
			}
		}

		[HttpGet("GetAllProducts")]
		public IActionResult GetAllProducts()
		{
			try
			{
				var products = _productService.GetAll();

				if (products == null || !products.Any()) return StatusCode((int)HttpStatusCode.NotFound);

				return Ok(products);
			}
			catch (Exception ex)
			{
				_logger.LogError(ex, $"Error on Getting All Products", null);
				return StatusCode((int)HttpStatusCode.InternalServerError, "Error on Getting All Products" + Environment.NewLine + ex.ToString());
			}
		}

		[HttpGet("GetAvailableProducts")]
		public IActionResult GetAvailableProducts(int page)
		{
			try
			{
				var products = _productService.GetAvailableProducts(page);

				if (products == null || !products.Items.Any()) return StatusCode((int)HttpStatusCode.NotFound);

				return Ok(products);
			}
			catch (Exception ex)
			{
				_logger.LogError(ex, $"Error on Getting Available Products", null);
				return StatusCode((int)HttpStatusCode.InternalServerError, "Error on Getting Available Products" + Environment.NewLine + ex.ToString());
			}
		}

		[HttpGet("GetFilteredProducts")]
		public IActionResult GetFilteredProducts(int page, string unitCode, string categoryCode, string typeCode)
		{
			try
			{
				var products = _productService.GetFilteredProducts(page, unitCode, categoryCode, typeCode);

				if (products == null || !products.Items.Any()) return StatusCode((int)HttpStatusCode.NotFound);

				return Ok(products);
			}
			catch (Exception ex)
			{
				_logger.LogError(ex, $"Error on Getting Filtered Products", null);
				return StatusCode((int)HttpStatusCode.InternalServerError, "Error on Getting Filtered Products" + Environment.NewLine + ex.ToString());
			}
		}

		[HttpPost("CreateProduct")]
		public IActionResult CreateProduct(ProductDto product)
		{
			try
			{
				_productService.Add(product);

				return Ok();
			}
			catch (Exception ex)
			{
				_logger.LogError(ex, $"Error Creating Product", product);
				return StatusCode((int)HttpStatusCode.InternalServerError, "Error on Creating Product" + Environment.NewLine + ex.ToString());
			}
		}

		[HttpPut("EditProduct")]
		public IActionResult EditProduct(ProductDto product)
		{
			try
			{
				_productService.Update(product);

				return Ok();
			}
			catch (Exception ex)
			{
				_logger.LogError(ex, $"Error Updating Product", product);
				return StatusCode((int)HttpStatusCode.InternalServerError, "Error on Updating Product" + Environment.NewLine + ex.ToString());
			}
		}
	}
}
